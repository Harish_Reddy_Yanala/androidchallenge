package  developer.focul.presentation.base;

import android.support.v4.app.Fragment;

import developer.focul.MainApplication;
import developer.focul.dagger.MainComponent;

public class BaseFragment extends Fragment {

    protected MainApplication getMainApplication() {
        return (MainApplication) getActivity().getApplication();
    }

    protected MainComponent getMainComponent() {
        return getMainApplication().getComponent();
    }
}
