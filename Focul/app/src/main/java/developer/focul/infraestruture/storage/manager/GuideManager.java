package  developer.focul.infraestruture.storage.manager;

import javax.inject.Inject;

import developer.focul.domain.aggregation.GuideAggregation;
import developer.focul.domain.repository.GuideRepository;
import developer.focul.infraestruture.operator.WorkerOperator;
import developer.focul.infraestruture.storage.client.GuideService;
import rx.Observable;

public class GuideManager implements GuideRepository {

    private final GuideService guideService;
    private final WorkerOperator<GuideAggregation> workerOperator;

    @Inject
    public GuideManager(GuideService guideService, WorkerOperator<GuideAggregation> workerOperator) {
        this.guideService = guideService;
        this.workerOperator = workerOperator;
    }

    @Override
    public Observable<GuideAggregation> getCompanies() {
        return guideService.getGuideAggregation().compose(workerOperator);
    }
}