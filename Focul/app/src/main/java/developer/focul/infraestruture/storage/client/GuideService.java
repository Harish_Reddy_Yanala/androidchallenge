package  developer.focul.infraestruture.storage.client;

import developer.focul.domain.aggregation.GuideAggregation;
import retrofit2.http.GET;
import rx.Observable;

public interface GuideService {

    @GET("items")
    Observable<GuideAggregation> getGuideAggregation();
}