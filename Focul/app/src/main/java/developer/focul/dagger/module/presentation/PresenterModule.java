package  developer.focul.dagger.module.presentation;

import developer.focul.dagger.scope.PerActivity;
import developer.focul.domain.repository.GuideRepository;
import developer.focul.presentation.ui.guidelist.GuideListContract;
import developer.focul.presentation.ui.guidelist.GuideListPresenter;
import dagger.Module;
import dagger.Provides;

@Module
public class PresenterModule {

    @PerActivity
    @Provides
    GuideListContract.Presenter provideMainPresenter(GuideRepository guideRepository) {
        return new GuideListPresenter(guideRepository);
    }
}
