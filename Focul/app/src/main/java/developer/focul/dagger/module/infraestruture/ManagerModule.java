package  developer.focul.dagger.module.infraestruture;

import javax.inject.Singleton;

import  developer.focul.domain.repository.GuideRepository;
import  developer.focul.infraestruture.operator.WorkerOperator;

import dagger.Module;
import dagger.Provides;
import  developer.focul.infraestruture.storage.client.GuideService;
import  developer.focul.infraestruture.storage.manager.GuideManager;

@Module
public class ManagerModule {

    @Singleton
    @Provides
    GuideRepository providesGuideRepository(GuideService guideService, WorkerOperator workerOperator) {
        return new GuideManager(guideService, workerOperator);
    }

}
