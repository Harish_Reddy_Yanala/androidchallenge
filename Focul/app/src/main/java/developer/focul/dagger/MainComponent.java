package  developer.focul.dagger;

import javax.inject.Singleton;

import  developer.focul.dagger.module.ApplicationModule;
import  developer.focul.dagger.module.PreferenceModule;
import  developer.focul.dagger.module.SettingModule;
import  developer.focul.dagger.module.domain.RepositoryModule;
import  developer.focul.dagger.module.infraestruture.ManagerModule;
import  developer.focul.dagger.module.infraestruture.NetworkModule;
import  developer.focul.dagger.module.infraestruture.RxJavaModule;
import  developer.focul.dagger.module.infraestruture.ServiceModule;
import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class, NetworkModule.class, PreferenceModule.class,
        RepositoryModule.class, SettingModule.class, RxJavaModule.class, ServiceModule.class,
        ManagerModule.class
})
public interface MainComponent {
    UiComponent uiComponent();
}
