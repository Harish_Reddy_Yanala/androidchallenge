package  developer.focul.dagger;

import developer.focul.dagger.module.presentation.PresenterModule;
import developer.focul.dagger.scope.PerActivity;
import developer.focul.presentation.ui.guidelist.GuideListActivity;
import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = {PresenterModule.class})
public interface UiComponent {
    void inject(GuideListActivity activity);
}
