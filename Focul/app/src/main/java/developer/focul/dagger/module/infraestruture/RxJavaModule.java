package  developer.focul.dagger.module.infraestruture;

import javax.inject.Singleton;

import developer.focul.infraestruture.operator.WorkerOperator;
import dagger.Module;
import dagger.Provides;

@Module
public class RxJavaModule {

    @Singleton
    @Provides
    WorkerOperator providesWorkerOperator() {
        return new WorkerOperator();
    }
}
