package  developer.focul.domain.repository;

import developer.focul.domain.aggregation.GuideAggregation;
import rx.Observable;

public interface GuideRepository {

    Observable<GuideAggregation> getCompanies();
}
