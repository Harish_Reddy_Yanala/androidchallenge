package  developer.focul.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Guide implements Parcelable {

    public static final Creator<Guide> CREATOR = new Creator<Guide>() {
        @Override
        public Guide createFromParcel(Parcel source) {
            return new Guide(source);
        }

        @Override
        public Guide[] newArray(int size) {
            return new Guide[size];
        }
    };
    public String _id;
    public String originalPrice;
    public String name;
    public String url;
    public String priority;
    public String icon;

    public Guide() {
    }

    protected Guide(Parcel in) {
        this._id = in.readString();
        this.originalPrice = in.readString();
        this.name = in.readString();
        this.url = in.readString();
        this.priority = in.readParcelable(Venue.class.getClassLoader());
        this.icon = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this._id);
        dest.writeString(this.originalPrice);
        dest.writeString(this.name);
        dest.writeString(this.url);
        dest.writeString(this.priority);
        dest.writeString(this.icon);
    }
}