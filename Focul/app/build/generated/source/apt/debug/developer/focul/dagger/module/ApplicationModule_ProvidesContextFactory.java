package developer.focul.dagger.module;

import android.app.Application;
import android.content.Context;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ApplicationModule_ProvidesContextFactory implements Factory<Context> {
  private final ApplicationModule module;

  private final Provider<Application> applicationProvider;

  public ApplicationModule_ProvidesContextFactory(
      ApplicationModule module, Provider<Application> applicationProvider) {
    assert module != null;
    this.module = module;
    assert applicationProvider != null;
    this.applicationProvider = applicationProvider;
  }

  @Override
  public Context get() {
    return Preconditions.checkNotNull(
        module.providesContext(applicationProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Context> create(
      ApplicationModule module, Provider<Application> applicationProvider) {
    return new ApplicationModule_ProvidesContextFactory(module, applicationProvider);
  }
}
