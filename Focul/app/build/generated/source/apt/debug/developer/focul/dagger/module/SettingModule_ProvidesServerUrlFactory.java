package developer.focul.dagger.module;

import android.content.Context;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class SettingModule_ProvidesServerUrlFactory implements Factory<String> {
  private final SettingModule module;

  private final Provider<Context> contextProvider;

  public SettingModule_ProvidesServerUrlFactory(
      SettingModule module, Provider<Context> contextProvider) {
    assert module != null;
    this.module = module;
    assert contextProvider != null;
    this.contextProvider = contextProvider;
  }

  @Override
  public String get() {
    return Preconditions.checkNotNull(
        module.providesServerUrl(contextProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<String> create(SettingModule module, Provider<Context> contextProvider) {
    return new SettingModule_ProvidesServerUrlFactory(module, contextProvider);
  }
}
