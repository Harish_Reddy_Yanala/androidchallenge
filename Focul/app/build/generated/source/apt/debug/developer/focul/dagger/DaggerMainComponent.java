package developer.focul.dagger;

import android.app.Application;
import android.content.Context;
import com.google.gson.Gson;
import dagger.MembersInjector;
import dagger.internal.DoubleCheck;
import dagger.internal.Preconditions;
import developer.focul.dagger.module.ApplicationModule;
import developer.focul.dagger.module.ApplicationModule_ProvidesApplicationFactory;
import developer.focul.dagger.module.ApplicationModule_ProvidesContextFactory;
import developer.focul.dagger.module.PreferenceModule;
import developer.focul.dagger.module.SettingModule;
import developer.focul.dagger.module.SettingModule_ProvidesServerUrlFactory;
import developer.focul.dagger.module.domain.RepositoryModule;
import developer.focul.dagger.module.infraestruture.ManagerModule;
import developer.focul.dagger.module.infraestruture.ManagerModule_ProvidesGuideRepositoryFactory;
import developer.focul.dagger.module.infraestruture.NetworkModule;
import developer.focul.dagger.module.infraestruture.NetworkModule_ProvidesGsonConverterFactoryFactory;
import developer.focul.dagger.module.infraestruture.NetworkModule_ProvidesGsonFactory;
import developer.focul.dagger.module.infraestruture.NetworkModule_ProvidesHttpLoggingInterceptorFactory;
import developer.focul.dagger.module.infraestruture.NetworkModule_ProvidesOkHttpCacheFactory;
import developer.focul.dagger.module.infraestruture.NetworkModule_ProvidesOkHttpClientFactory;
import developer.focul.dagger.module.infraestruture.NetworkModule_ProvidesRetrofitFactory;
import developer.focul.dagger.module.infraestruture.NetworkModule_ProvidesRxJavaCallAdapterFactoryFactory;
import developer.focul.dagger.module.infraestruture.RxJavaModule;
import developer.focul.dagger.module.infraestruture.RxJavaModule_ProvidesWorkerOperatorFactory;
import developer.focul.dagger.module.infraestruture.ServiceModule;
import developer.focul.dagger.module.infraestruture.ServiceModule_ProvidesGuideServiceFactory;
import developer.focul.dagger.module.presentation.PresenterModule;
import developer.focul.dagger.module.presentation.PresenterModule_ProvideMainPresenterFactory;
import developer.focul.domain.repository.GuideRepository;
import developer.focul.infraestruture.operator.WorkerOperator;
import developer.focul.infraestruture.storage.client.GuideService;
import developer.focul.presentation.ui.guidelist.GuideListActivity;
import developer.focul.presentation.ui.guidelist.GuideListActivity_MembersInjector;
import developer.focul.presentation.ui.guidelist.GuideListContract;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerMainComponent implements MainComponent {
  private Provider<Gson> providesGsonProvider;

  private Provider<GsonConverterFactory> providesGsonConverterFactoryProvider;

  private Provider<Application> providesApplicationProvider;

  private Provider<Cache> providesOkHttpCacheProvider;

  private Provider<HttpLoggingInterceptor> providesHttpLoggingInterceptorProvider;

  private Provider<OkHttpClient> providesOkHttpClientProvider;

  private Provider<RxJavaCallAdapterFactory> providesRxJavaCallAdapterFactoryProvider;

  private Provider<Context> providesContextProvider;

  private Provider<String> providesServerUrlProvider;

  private Provider<Retrofit> providesRetrofitProvider;

  private Provider<GuideService> providesGuideServiceProvider;

  private Provider<WorkerOperator> providesWorkerOperatorProvider;

  private Provider<GuideRepository> providesGuideRepositoryProvider;

  private DaggerMainComponent(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.providesGsonProvider =
        DoubleCheck.provider(NetworkModule_ProvidesGsonFactory.create(builder.networkModule));

    this.providesGsonConverterFactoryProvider =
        DoubleCheck.provider(
            NetworkModule_ProvidesGsonConverterFactoryFactory.create(
                builder.networkModule, providesGsonProvider));

    this.providesApplicationProvider =
        DoubleCheck.provider(
            ApplicationModule_ProvidesApplicationFactory.create(builder.applicationModule));

    this.providesOkHttpCacheProvider =
        DoubleCheck.provider(
            NetworkModule_ProvidesOkHttpCacheFactory.create(
                builder.networkModule, providesApplicationProvider));

    this.providesHttpLoggingInterceptorProvider =
        DoubleCheck.provider(
            NetworkModule_ProvidesHttpLoggingInterceptorFactory.create(builder.networkModule));

    this.providesOkHttpClientProvider =
        DoubleCheck.provider(
            NetworkModule_ProvidesOkHttpClientFactory.create(
                builder.networkModule,
                providesOkHttpCacheProvider,
                providesHttpLoggingInterceptorProvider));

    this.providesRxJavaCallAdapterFactoryProvider =
        DoubleCheck.provider(
            NetworkModule_ProvidesRxJavaCallAdapterFactoryFactory.create(builder.networkModule));

    this.providesContextProvider =
        DoubleCheck.provider(
            ApplicationModule_ProvidesContextFactory.create(
                builder.applicationModule, providesApplicationProvider));

    this.providesServerUrlProvider =
        DoubleCheck.provider(
            SettingModule_ProvidesServerUrlFactory.create(
                builder.settingModule, providesContextProvider));

    this.providesRetrofitProvider =
        DoubleCheck.provider(
            NetworkModule_ProvidesRetrofitFactory.create(
                builder.networkModule,
                providesGsonConverterFactoryProvider,
                providesOkHttpClientProvider,
                providesRxJavaCallAdapterFactoryProvider,
                providesServerUrlProvider));

    this.providesGuideServiceProvider =
        DoubleCheck.provider(
            ServiceModule_ProvidesGuideServiceFactory.create(
                builder.serviceModule, providesRetrofitProvider));

    this.providesWorkerOperatorProvider =
        DoubleCheck.provider(
            RxJavaModule_ProvidesWorkerOperatorFactory.create(builder.rxJavaModule));

    this.providesGuideRepositoryProvider =
        DoubleCheck.provider(
            ManagerModule_ProvidesGuideRepositoryFactory.create(
                builder.managerModule,
                providesGuideServiceProvider,
                providesWorkerOperatorProvider));
  }

  @Override
  public UiComponent uiComponent() {
    return new UiComponentImpl();
  }

  public static final class Builder {
    private NetworkModule networkModule;

    private ApplicationModule applicationModule;

    private SettingModule settingModule;

    private ServiceModule serviceModule;

    private RxJavaModule rxJavaModule;

    private ManagerModule managerModule;

    private Builder() {}

    public MainComponent build() {
      if (networkModule == null) {
        this.networkModule = new NetworkModule();
      }
      if (applicationModule == null) {
        throw new IllegalStateException(
            ApplicationModule.class.getCanonicalName() + " must be set");
      }
      if (settingModule == null) {
        this.settingModule = new SettingModule();
      }
      if (serviceModule == null) {
        this.serviceModule = new ServiceModule();
      }
      if (rxJavaModule == null) {
        this.rxJavaModule = new RxJavaModule();
      }
      if (managerModule == null) {
        this.managerModule = new ManagerModule();
      }
      return new DaggerMainComponent(this);
    }

    public Builder applicationModule(ApplicationModule applicationModule) {
      this.applicationModule = Preconditions.checkNotNull(applicationModule);
      return this;
    }

    public Builder networkModule(NetworkModule networkModule) {
      this.networkModule = Preconditions.checkNotNull(networkModule);
      return this;
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This method is a no-op. For more, see https://google.github.io/dagger/unused-modules.
     */
    @Deprecated
    public Builder preferenceModule(PreferenceModule preferenceModule) {
      Preconditions.checkNotNull(preferenceModule);
      return this;
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This method is a no-op. For more, see https://google.github.io/dagger/unused-modules.
     */
    @Deprecated
    public Builder repositoryModule(RepositoryModule repositoryModule) {
      Preconditions.checkNotNull(repositoryModule);
      return this;
    }

    public Builder settingModule(SettingModule settingModule) {
      this.settingModule = Preconditions.checkNotNull(settingModule);
      return this;
    }

    public Builder rxJavaModule(RxJavaModule rxJavaModule) {
      this.rxJavaModule = Preconditions.checkNotNull(rxJavaModule);
      return this;
    }

    public Builder serviceModule(ServiceModule serviceModule) {
      this.serviceModule = Preconditions.checkNotNull(serviceModule);
      return this;
    }

    public Builder managerModule(ManagerModule managerModule) {
      this.managerModule = Preconditions.checkNotNull(managerModule);
      return this;
    }
  }

  private final class UiComponentImpl implements UiComponent {
    private final PresenterModule presenterModule;

    private Provider<GuideListContract.Presenter> provideMainPresenterProvider;

    private MembersInjector<GuideListActivity> guideListActivityMembersInjector;

    private UiComponentImpl() {
      this.presenterModule = new PresenterModule();
      initialize();
    }

    @SuppressWarnings("unchecked")
    private void initialize() {

      this.provideMainPresenterProvider =
          DoubleCheck.provider(
              PresenterModule_ProvideMainPresenterFactory.create(
                  presenterModule, DaggerMainComponent.this.providesGuideRepositoryProvider));

      this.guideListActivityMembersInjector =
          GuideListActivity_MembersInjector.create(provideMainPresenterProvider);
    }

    @Override
    public void inject(GuideListActivity activity) {
      guideListActivityMembersInjector.injectMembers(activity);
    }
  }
}
