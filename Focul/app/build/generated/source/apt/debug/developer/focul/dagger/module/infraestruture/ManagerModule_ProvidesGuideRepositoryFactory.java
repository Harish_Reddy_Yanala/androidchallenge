package developer.focul.dagger.module.infraestruture;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import developer.focul.domain.repository.GuideRepository;
import developer.focul.infraestruture.operator.WorkerOperator;
import developer.focul.infraestruture.storage.client.GuideService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ManagerModule_ProvidesGuideRepositoryFactory
    implements Factory<GuideRepository> {
  private final ManagerModule module;

  private final Provider<GuideService> guideServiceProvider;

  private final Provider<WorkerOperator> workerOperatorProvider;

  public ManagerModule_ProvidesGuideRepositoryFactory(
      ManagerModule module,
      Provider<GuideService> guideServiceProvider,
      Provider<WorkerOperator> workerOperatorProvider) {
    assert module != null;
    this.module = module;
    assert guideServiceProvider != null;
    this.guideServiceProvider = guideServiceProvider;
    assert workerOperatorProvider != null;
    this.workerOperatorProvider = workerOperatorProvider;
  }

  @Override
  public GuideRepository get() {
    return Preconditions.checkNotNull(
        module.providesGuideRepository(guideServiceProvider.get(), workerOperatorProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<GuideRepository> create(
      ManagerModule module,
      Provider<GuideService> guideServiceProvider,
      Provider<WorkerOperator> workerOperatorProvider) {
    return new ManagerModule_ProvidesGuideRepositoryFactory(
        module, guideServiceProvider, workerOperatorProvider);
  }
}
