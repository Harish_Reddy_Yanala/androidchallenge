package developer.focul.dagger.module.presentation;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import developer.focul.domain.repository.GuideRepository;
import developer.focul.presentation.ui.guidelist.GuideListContract;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PresenterModule_ProvideMainPresenterFactory
    implements Factory<GuideListContract.Presenter> {
  private final PresenterModule module;

  private final Provider<GuideRepository> guideRepositoryProvider;

  public PresenterModule_ProvideMainPresenterFactory(
      PresenterModule module, Provider<GuideRepository> guideRepositoryProvider) {
    assert module != null;
    this.module = module;
    assert guideRepositoryProvider != null;
    this.guideRepositoryProvider = guideRepositoryProvider;
  }

  @Override
  public GuideListContract.Presenter get() {
    return Preconditions.checkNotNull(
        module.provideMainPresenter(guideRepositoryProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<GuideListContract.Presenter> create(
      PresenterModule module, Provider<GuideRepository> guideRepositoryProvider) {
    return new PresenterModule_ProvideMainPresenterFactory(module, guideRepositoryProvider);
  }
}
