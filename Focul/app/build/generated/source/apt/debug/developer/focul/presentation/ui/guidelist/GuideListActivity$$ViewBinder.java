// Generated code from Butter Knife. Do not modify!
package developer.focul.presentation.ui.guidelist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Finder;
import butterknife.internal.ViewBinder;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.lang.Override;

public class GuideListActivity$$ViewBinder<T extends GuideListActivity> implements ViewBinder<T> {
  @Override
  public Unbinder bind(Finder finder, T target, Object source) {
    return new InnerUnbinder<>(target, finder, source);
  }

  protected static class InnerUnbinder<T extends GuideListActivity> implements Unbinder {
    protected T target;

    private View view2131492985;

    protected InnerUnbinder(final T target, Finder finder, Object source) {
      this.target = target;

      View view;
      target.guideList = finder.findRequiredViewAsType(source, 2131492973, "field 'guideList'", RecyclerView.class);
      target.successContainer = finder.findRequiredViewAsType(source, 2131492972, "field 'successContainer'", LinearLayout.class);
      target.errorContainer = finder.findRequiredViewAsType(source, 2131492984, "field 'errorContainer'", LinearLayout.class);
      target.loadingContainer = finder.findRequiredViewAsType(source, 2131492986, "field 'loadingContainer'", LinearLayout.class);
      target.emptyContainer = finder.findRequiredViewAsType(source, 2131492983, "field 'emptyContainer'", LinearLayout.class);
      view = finder.findRequiredView(source, 2131492985, "method 'retryGetCompanies'");
      view2131492985 = view;
      view.setOnClickListener(new DebouncingOnClickListener() {
        @Override
        public void doClick(View p0) {
          target.retryGetCompanies();
        }
      });
    }

    @Override
    public void unbind() {
      T target = this.target;
      if (target == null) throw new IllegalStateException("Bindings already cleared.");

      target.guideList = null;
      target.successContainer = null;
      target.errorContainer = null;
      target.loadingContainer = null;
      target.emptyContainer = null;

      view2131492985.setOnClickListener(null);
      view2131492985 = null;

      this.target = null;
    }
  }
}
