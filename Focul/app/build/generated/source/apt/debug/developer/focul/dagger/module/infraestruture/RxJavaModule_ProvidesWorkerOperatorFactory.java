package developer.focul.dagger.module.infraestruture;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import developer.focul.infraestruture.operator.WorkerOperator;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class RxJavaModule_ProvidesWorkerOperatorFactory implements Factory<WorkerOperator> {
  private final RxJavaModule module;

  public RxJavaModule_ProvidesWorkerOperatorFactory(RxJavaModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public WorkerOperator get() {
    return Preconditions.checkNotNull(
        module.providesWorkerOperator(),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<WorkerOperator> create(RxJavaModule module) {
    return new RxJavaModule_ProvidesWorkerOperatorFactory(module);
  }
}
