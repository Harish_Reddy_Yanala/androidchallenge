package developer.focul.dagger.module.infraestruture;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NetworkModule_ProvidesRetrofitFactory implements Factory<Retrofit> {
  private final NetworkModule module;

  private final Provider<GsonConverterFactory> gsonConverterFactoryProvider;

  private final Provider<OkHttpClient> okHttpClientProvider;

  private final Provider<RxJavaCallAdapterFactory> rxJavaCallAdapterFactoryProvider;

  private final Provider<String> baseUrlProvider;

  public NetworkModule_ProvidesRetrofitFactory(
      NetworkModule module,
      Provider<GsonConverterFactory> gsonConverterFactoryProvider,
      Provider<OkHttpClient> okHttpClientProvider,
      Provider<RxJavaCallAdapterFactory> rxJavaCallAdapterFactoryProvider,
      Provider<String> baseUrlProvider) {
    assert module != null;
    this.module = module;
    assert gsonConverterFactoryProvider != null;
    this.gsonConverterFactoryProvider = gsonConverterFactoryProvider;
    assert okHttpClientProvider != null;
    this.okHttpClientProvider = okHttpClientProvider;
    assert rxJavaCallAdapterFactoryProvider != null;
    this.rxJavaCallAdapterFactoryProvider = rxJavaCallAdapterFactoryProvider;
    assert baseUrlProvider != null;
    this.baseUrlProvider = baseUrlProvider;
  }

  @Override
  public Retrofit get() {
    return Preconditions.checkNotNull(
        module.providesRetrofit(
            gsonConverterFactoryProvider.get(),
            okHttpClientProvider.get(),
            rxJavaCallAdapterFactoryProvider.get(),
            baseUrlProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Retrofit> create(
      NetworkModule module,
      Provider<GsonConverterFactory> gsonConverterFactoryProvider,
      Provider<OkHttpClient> okHttpClientProvider,
      Provider<RxJavaCallAdapterFactory> rxJavaCallAdapterFactoryProvider,
      Provider<String> baseUrlProvider) {
    return new NetworkModule_ProvidesRetrofitFactory(
        module,
        gsonConverterFactoryProvider,
        okHttpClientProvider,
        rxJavaCallAdapterFactoryProvider,
        baseUrlProvider);
  }
}
