package developer.focul.dagger.module.infraestruture;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NetworkModule_ProvidesRxJavaCallAdapterFactoryFactory
    implements Factory<RxJavaCallAdapterFactory> {
  private final NetworkModule module;

  public NetworkModule_ProvidesRxJavaCallAdapterFactoryFactory(NetworkModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public RxJavaCallAdapterFactory get() {
    return Preconditions.checkNotNull(
        module.providesRxJavaCallAdapterFactory(),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<RxJavaCallAdapterFactory> create(NetworkModule module) {
    return new NetworkModule_ProvidesRxJavaCallAdapterFactoryFactory(module);
  }
}
