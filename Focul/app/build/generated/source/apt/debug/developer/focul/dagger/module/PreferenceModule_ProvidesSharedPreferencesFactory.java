package developer.focul.dagger.module;

import android.content.Context;
import android.content.SharedPreferences;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PreferenceModule_ProvidesSharedPreferencesFactory
    implements Factory<SharedPreferences> {
  private final PreferenceModule module;

  private final Provider<Context> contextProvider;

  public PreferenceModule_ProvidesSharedPreferencesFactory(
      PreferenceModule module, Provider<Context> contextProvider) {
    assert module != null;
    this.module = module;
    assert contextProvider != null;
    this.contextProvider = contextProvider;
  }

  @Override
  public SharedPreferences get() {
    return Preconditions.checkNotNull(
        module.providesSharedPreferences(contextProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<SharedPreferences> create(
      PreferenceModule module, Provider<Context> contextProvider) {
    return new PreferenceModule_ProvidesSharedPreferencesFactory(module, contextProvider);
  }
}
