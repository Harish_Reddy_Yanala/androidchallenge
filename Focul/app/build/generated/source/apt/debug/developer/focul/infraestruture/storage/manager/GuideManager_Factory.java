package developer.focul.infraestruture.storage.manager;

import dagger.internal.Factory;
import developer.focul.domain.aggregation.GuideAggregation;
import developer.focul.infraestruture.operator.WorkerOperator;
import developer.focul.infraestruture.storage.client.GuideService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class GuideManager_Factory implements Factory<GuideManager> {
  private final Provider<GuideService> guideServiceProvider;

  private final Provider<WorkerOperator<GuideAggregation>> workerOperatorProvider;

  public GuideManager_Factory(
      Provider<GuideService> guideServiceProvider,
      Provider<WorkerOperator<GuideAggregation>> workerOperatorProvider) {
    assert guideServiceProvider != null;
    this.guideServiceProvider = guideServiceProvider;
    assert workerOperatorProvider != null;
    this.workerOperatorProvider = workerOperatorProvider;
  }

  @Override
  public GuideManager get() {
    return new GuideManager(guideServiceProvider.get(), workerOperatorProvider.get());
  }

  public static Factory<GuideManager> create(
      Provider<GuideService> guideServiceProvider,
      Provider<WorkerOperator<GuideAggregation>> workerOperatorProvider) {
    return new GuideManager_Factory(guideServiceProvider, workerOperatorProvider);
  }
}
