package developer.focul.presentation.ui.guidelist;

import dagger.internal.Factory;
import developer.focul.domain.repository.GuideRepository;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class GuideListPresenter_Factory implements Factory<GuideListPresenter> {
  private final Provider<GuideRepository> repositoryProvider;

  public GuideListPresenter_Factory(Provider<GuideRepository> repositoryProvider) {
    assert repositoryProvider != null;
    this.repositoryProvider = repositoryProvider;
  }

  @Override
  public GuideListPresenter get() {
    return new GuideListPresenter(repositoryProvider.get());
  }

  public static Factory<GuideListPresenter> create(Provider<GuideRepository> repositoryProvider) {
    return new GuideListPresenter_Factory(repositoryProvider);
  }
}
