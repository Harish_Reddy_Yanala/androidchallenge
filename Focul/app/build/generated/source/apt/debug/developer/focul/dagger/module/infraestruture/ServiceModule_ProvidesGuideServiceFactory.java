package developer.focul.dagger.module.infraestruture;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import developer.focul.infraestruture.storage.client.GuideService;
import javax.annotation.Generated;
import javax.inject.Provider;
import retrofit2.Retrofit;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ServiceModule_ProvidesGuideServiceFactory implements Factory<GuideService> {
  private final ServiceModule module;

  private final Provider<Retrofit> retrofitProvider;

  public ServiceModule_ProvidesGuideServiceFactory(
      ServiceModule module, Provider<Retrofit> retrofitProvider) {
    assert module != null;
    this.module = module;
    assert retrofitProvider != null;
    this.retrofitProvider = retrofitProvider;
  }

  @Override
  public GuideService get() {
    return Preconditions.checkNotNull(
        module.providesGuideService(retrofitProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<GuideService> create(
      ServiceModule module, Provider<Retrofit> retrofitProvider) {
    return new ServiceModule_ProvidesGuideServiceFactory(module, retrofitProvider);
  }
}
