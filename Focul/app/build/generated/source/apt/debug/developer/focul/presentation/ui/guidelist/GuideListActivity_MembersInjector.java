package developer.focul.presentation.ui.guidelist;

import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class GuideListActivity_MembersInjector implements MembersInjector<GuideListActivity> {
  private final Provider<GuideListContract.Presenter> presenterProvider;

  public GuideListActivity_MembersInjector(
      Provider<GuideListContract.Presenter> presenterProvider) {
    assert presenterProvider != null;
    this.presenterProvider = presenterProvider;
  }

  public static MembersInjector<GuideListActivity> create(
      Provider<GuideListContract.Presenter> presenterProvider) {
    return new GuideListActivity_MembersInjector(presenterProvider);
  }

  @Override
  public void injectMembers(GuideListActivity instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.presenter = presenterProvider.get();
  }

  public static void injectPresenter(
      GuideListActivity instance, Provider<GuideListContract.Presenter> presenterProvider) {
    instance.presenter = presenterProvider.get();
  }
}
