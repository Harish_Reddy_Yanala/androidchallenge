package developer.focul.dagger.module.infraestruture;

import com.google.gson.Gson;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;
import retrofit2.converter.gson.GsonConverterFactory;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class NetworkModule_ProvidesGsonConverterFactoryFactory
    implements Factory<GsonConverterFactory> {
  private final NetworkModule module;

  private final Provider<Gson> gsonProvider;

  public NetworkModule_ProvidesGsonConverterFactoryFactory(
      NetworkModule module, Provider<Gson> gsonProvider) {
    assert module != null;
    this.module = module;
    assert gsonProvider != null;
    this.gsonProvider = gsonProvider;
  }

  @Override
  public GsonConverterFactory get() {
    return Preconditions.checkNotNull(
        module.providesGsonConverterFactory(gsonProvider.get()),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<GsonConverterFactory> create(
      NetworkModule module, Provider<Gson> gsonProvider) {
    return new NetworkModule_ProvidesGsonConverterFactoryFactory(module, gsonProvider);
  }
}
